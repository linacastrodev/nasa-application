# [Conexao Galápagos website]

##DOCKING
The current situation is clunky, high risk and high precision.
What if we re-imagine a docking system for the future (say, 20 years from now) which allows for more margin of error, is more flexible, automated, and able to help building spaceships off-planet? 
I propose we look at Nature for simpler solutions.


This project was developed with HTML5, Sass, jQuery, and Hammer.js.
Performance basics are covered: assets are minified into single CSS and JS files, and the images are optimized.

[LIVE PREVIEW](https://linacastrodev.gitlab.io/nasa-application/)

## Misc:

* NASA DATA
* NASA IMAGES
* The original PSD is included and was provided by [Sergey Melnik](https://www.behance.net/SergeyMelnik).
